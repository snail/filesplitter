/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filespliter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ExampleMode;
import org.kohsuke.args4j.Option;

/**
 *
 * @author Administrator
 */
public class FileSpliterArgs {

    @Option(name = "-f", usage = "source file", required = true, metaVar = "filename")
    public File file;
    @Option(name = "-d", usage = "output dir,default:source file directory", metaVar = "directory")
    public String dir;
    @Option(name = "-s", usage = "block size,such as 1k,1m,1g", required = true, metaVar = "size")
    public String size = "1m";
    @Option(name = "-lf", usage = "split by line end")
    public boolean withLine = false;
    @Option(name = "-h", usage = "show this help")
    public boolean help = false;
    public long blockSize = 0;
    final public static HashMap<String, Integer> sizeMap = new HashMap<String, Integer>();

    static {
        sizeMap.put("k", 1024);
        sizeMap.put("m", 1024 * 1024);
        sizeMap.put("g", 1024 * 1024 * 1024);
    }
    @Argument
    public List<String> arguments = new ArrayList<String>();

    public static void main(String args[]) {
        new FileSpliterArgs().doParse(args);
    }

    public void doParse(String args[]) {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);
            if (help || !size.toLowerCase().matches("\\d+(?:k|m|g)")) {
                usage(parser);
            } else {
                int sizeLen = size.length();
                blockSize = Integer.parseInt(size.substring(0, sizeLen - 1)) * sizeMap.get(size.substring(sizeLen - 1, sizeLen).toLowerCase());
                if (dir == null) {
                    dir = file.getParent() + File.separator;
                }
                new FileSpliter(this);
            }
        } catch (Exception e) {
            if (!help) {
                System.out.println(e.getMessage());
            }
            usage(parser);
        }
    }

    public void usage(CmdLineParser parser) {
        System.out.println("Usage:");
        parser.printUsage(System.out);
        System.out.println("Example:" + parser.printExample(ExampleMode.ALL));
        System.out.println("Example:" + parser.printExample(ExampleMode.REQUIRED));
    }
}
